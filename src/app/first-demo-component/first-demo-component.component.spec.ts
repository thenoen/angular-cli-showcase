import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstDemoComponentComponent } from './first-demo-component.component';

describe('FirstDemoComponentComponent', () => {
  let component: FirstDemoComponentComponent;
  let fixture: ComponentFixture<FirstDemoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstDemoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstDemoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
