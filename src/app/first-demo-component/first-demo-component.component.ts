import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-first-demo-component',
  templateUrl: './first-demo-component.component.html',
  styleUrls: ['./first-demo-component.component.css']
})
export class FirstDemoComponentComponent implements OnInit {

  time: string;
  private apiUrl = '/data'

  constructor(private http: Http) { }

  ngOnInit() {
    console.log("running onInit");
    this.http.get(this.apiUrl).subscribe(x => this.handleResponse(x));
  }

  private handleResponse(response: Response) {
    console.log(response);
    this.time = response.text();
  }

}
