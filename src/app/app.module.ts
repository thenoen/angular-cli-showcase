import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstDemoComponentComponent } from './first-demo-component/first-demo-component.component';

import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    FirstDemoComponentComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
