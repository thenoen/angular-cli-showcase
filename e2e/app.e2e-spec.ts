import { AngularCliShowcasePage } from './app.po';

describe('angular-cli-showcase App', () => {
  let page: AngularCliShowcasePage;

  beforeEach(() => {
    page = new AngularCliShowcasePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
